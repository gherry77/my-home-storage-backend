<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use DateInterval;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
#[ApiResource]
class Product
{
    public const EXPIRING_SOON_DAYS = 6;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show_product", "list_product"})
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show_product", "list_product"})
     * @Assert\NotBlank
     */
    private string $description;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"show_product", "list_product"})
     * @Assert\PositiveOrZero
     */
    private string $quantity;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"show_product", "list_product"})
     *
     * @var ?\DateTimeInterface
     */
    private $expiryDate = null;

    /**
     * @ORM\ManyToOne(targetEntity=Storage::class, inversedBy="products")
     * @Groups({"show_product", "list_product"})
     */
    private ?Storage $storage = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getExpiryDate(): ?\DateTimeInterface
    {
        return $this->expiryDate;
    }

    public function setExpiryDate(?\DateTimeInterface $expiryDate): self
    {
        $this->expiryDate = $expiryDate;

        return $this;
    }

    public function isExpired(): bool
    {
        return $this->getExpiryDate() < new \DateTime();
    }

    public function isExpiringSoon(): bool
    {
        return $this->getExpiryDate() >= new \DateTime()
            && $this->getExpiryDate() <= (new \DateTime())->add(new DateInterval('P'.self::EXPIRING_SOON_DAYS.'D'));
    }

    public function getExpiringDays(): ?int
    {
        $expiryDate = $this->getExpiryDate();
        if (null === $expiryDate) {
            return null;
        }

        $today = new \DateTime();
        $interval = $today->diff($expiryDate);
        $expiringDays = $interval->format('%r%a');
        //$expiringDays = $interval->days;

        return (int) $expiringDays;
    }

    public function getStorage(): ?Storage
    {
        return $this->storage;
    }

    public function setStorage(?Storage $storage): self
    {
        $this->storage = $storage;

        return $this;
    }
}
