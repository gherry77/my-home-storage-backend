<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;

class StorageStatistics
{
    public int $expired = 0;
    public int $expiring_soon = 0;
    public int $not_expired = 0;
    public int $total = 0;

    /** @var Product[]|null */
    private $products;

    /**
     * @param Product[]|null $products
     */
    public function __construct($products = null)
    {
        $this->products = $products;

        if ($products) {
            $this->buildStatistics($products);
        }
    }

    public function getStatistics(): string|false
    {
        return json_encode($this);
    }

    /**
     * @param Product[]|null $products
     */
    protected function buildStatistics($products = null): void
    {
        foreach ($products as $product) {
            if ($product->isExpired()) {
                ++$this->expired;
            } elseif ($product->isExpiringSoon()) {
                ++$this->expiring_soon;
            } else {
                ++$this->not_expired;
            }

            ++$this->total;
        }
    }
}
