<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;

class ProductReport
{
    /** @var Product[]|null */
    private $products;

    /** @var Product[]|null */
    private $sortedProducts = [];

    /**
     * @param Product[]|null $products
     */
    public function __construct($products = null)
    {
        $this->products = $products;

        if ($products) {
            $this->buildReport($products);
        }
    }

    /**
     * @return Product[]|null
     */
    public function getReport()
    {
        return $this->sortedProducts;
    }

    /**
     * @param Product[]|null $products
     */
    protected function buildReport($products): void
    {
        // order products by: expiringDays ASC, description ASC
        usort($products, function (Product $a, Product $b): int {
            if ($a->getExpiringDays() === $b->getExpiringDays()) {
                return $a->getDescription() <=> $b->getDescription();
            }

            return $a->getExpiringDays() <=> $b->getExpiringDays();
        });

        foreach ($products as $product) {
            if (null !== $product->getExpiryDate()) {
                $this->sortedProducts[] = $product;
            }
        }

        foreach ($products as $product) {
            if (null === $product->getExpiryDate()) {
                $this->sortedProducts[] = $product;
            }
        }
    }
}
