<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Storage;
use DateInterval;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        /** FRIGO */
        $s1 = new Storage();
        $s1->setName('frigo');
        $manager->persist($s1);

        $p1 = new Product();
        $p1->setDescription('Uova');
        $p1->setQuantity('2');
        $p1->setExpiryDate((new \DateTime())->add(new DateInterval('P2D')));
        $p1->setStorage($s1);
        $manager->persist($p1);

        $p2 = new Product();
        $p2->setDescription('Insalata');
        $p2->setQuantity('1');
        $p2->setExpiryDate((new \DateTime())->sub(new DateInterval('P1D')));
        $p2->setStorage($s1);
        $manager->persist($p2);

        /** DISPENSA */
        $s2 = new Storage();
        $s2->setName('dispensa');
        $manager->persist($s2);

        $p3 = new Product();
        $p3->setDescription('Pasta');
        $p3->setQuantity('3');
        $p3->setExpiryDate((new \DateTime())->add(new DateInterval('P60D')));
        $p3->setStorage($s2);
        $manager->persist($p3);

        $manager->flush();
    }
}
