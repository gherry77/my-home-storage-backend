<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Storage;
use App\Exception\StorageNotEmptyException;
use App\Form\StorageFormType;
use App\Service\ProductReport;
use App\Service\StorageStatistics;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StorageController extends BaseController
{
    #[Route('/storages/{id}/products', name: 'storage__get_products', methods: ['GET'])]
    public function showProducts(int $id, Request $request): Response
    {
        $description = $request->query->get('description');

        $storage = $this->storageRepository->find($id);
        if (null === $storage) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $products = $this->productRepository->findByDescriptionAndStorage(
            $description,
            $storage
        );

        $productReport = new ProductReport($products);

        return $this->JsonProductResponse($productReport->getReport());

        // return $this->JsonProductResponse($products);
    }

    #[Route('/storages/{id}/statistics', name: 'storage_statistics', methods: ['GET'])]
    public function statistics(int $id): Response
    {
        $storage = $this->storageRepository->find($id);
        if (null === $storage) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $products = $this->productRepository->findBy(['storage' => $id]);

        $storageStatistics = new StorageStatistics($products);

        return new JsonResponse($storageStatistics->getStatistics(), Response::HTTP_OK, [], true);
    }

    #[Route('/storages', name: 'storages_get_collection', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $storages = $this->storageRepository->findAll();

        return $this->JsonStorageResponse($storages);
    }

    #[Route('/storages', name: 'storage_post', methods: ['POST'])]
    public function post(Request $request): Response
    {
        $storage = new Storage();

        return $this->manageStorageRequest($storage, $request);
    }

    #[Route('/storages/{id}', name: 'storage', methods: ['GET', 'PUT', 'DELETE'])]
    public function show(int $id, Request $request): Response
    {
        $storage = $this->storageRepository->find($id);
        if (null === $storage) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        if ('PUT' === $request->getMethod()) {
            return $this->manageStorageRequest($storage, $request);
        }

        if ('DELETE' === $request->getMethod()) {
            $products = $storage->getProducts();

            if (\count($products)) {
                throw new StorageNotEmptyException();
            }

            $this->entityManager->remove($storage);
            $this->entityManager->flush();

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        }

        return $this->JsonStorageResponse($storage);
    }

    private function manageStorageRequest(Storage $storage, Request $request): Response
    {
        $form = $this->createForm(StorageFormType::class, $storage);

        // Decode de JSON input
        $data = json_decode($request->getContent(), true);

        // Post the data to the form
        $form->submit($data);

        if ($form->isValid()) {
            $storage = $form->getData();
            $this->entityManager->persist($storage);
            $this->entityManager->flush();

            return $this->JsonStorageResponse($storage);
        }

        return $this->JsonFormResponse($form, Response::HTTP_BAD_REQUEST);
    }
}
