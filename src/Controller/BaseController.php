<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\ProductRepository;
use App\Repository\StorageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class BaseController extends AbstractController
{
    protected EntityManagerInterface $entityManager;

    protected SerializerInterface $serializer;

    protected ProductRepository $productRepository;

    protected StorageRepository $storageRepository;

    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository,
        StorageRepository $storageRepository
    ) {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->productRepository = $productRepository;
        $this->storageRepository = $storageRepository;
    }

    public function JsonFormResponse(mixed $data = null, int $status = 200, array $headers = []): JsonResponse
    {
        $jsonContent = $this->serializer->serialize($data, 'json');

        return new JsonResponse($jsonContent, $status, $headers, true);
    }

    /**
     * Return Json Response using Serializer.
     */
    public function JsonProductResponse(
        mixed $data = null,
        int $status = 200,
        array $headers = [],
        array $normalizeContext = []
    ): Response {
        $normalizeContext = array_merge(
            [AbstractNormalizer::ATTRIBUTES => ['id', 'description', 'quantity', 'expiryDate', 'expiringDays', 'storage' => ['id', 'name']]],
            $normalizeContext
        );

        $dateCallback = function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
            return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d') : '';
        };

        $defaultContext = [
            AbstractNormalizer::CALLBACKS => [
                'expiryDate' => $dateCallback,
            ],
        ];

        $normalizer = new GetSetMethodNormalizer(null, new CamelCaseToSnakeCaseNameConverter(), null, null, null, $defaultContext);

        $encoders = [new JsonEncoder()];
        $normalizers = [$normalizer];

        $serializer = new Serializer($normalizers, $encoders);
        $dataNormalized = $serializer->normalize($data, null, $normalizeContext);

        return new JsonResponse($dataNormalized, $status, $headers);

        // $jsonContent = $this->serializer->serialize($data, 'json');
        // return new JsonResponse($jsonContent, $status, $headers, true);
    }

    /**
     * Return Json Response using Serializer.
     */
    public function JsonStorageResponse(mixed $data = null, int $status = 200, array $headers = []): Response
    {
        $jsonContent = $this->serializer->serialize($data, 'json', [AbstractNormalizer::ATTRIBUTES => ['id', 'name']]);

        return new JsonResponse($jsonContent, $status, $headers, true);
    }
}
