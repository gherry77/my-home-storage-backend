<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductFormType;
use App\Service\ProductReport;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends BaseController
{
    #[Route('/products', name: 'products_get_collection', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $description = $request->query->get('description');
        $storageId = $request->query->get('storage');

        $storage = null;
        if ($storageId) {
            $storage = $this->storageRepository->find($storageId);
        }

        $products = $this->productRepository->findByDescriptionAndStorage($description, $storage);

        $productReport = new ProductReport($products);

        return $this->JsonProductResponse($productReport->getReport());
    }

    #[Route('/products', name: 'product_post', methods: ['POST'])]
    public function post(Request $request): Response
    {
        $product = new Product();

        return $this->manageProductRequest($product, $request);
    }

    #[Route('/products/{id}', name: 'product', methods: ['GET', 'PUT', 'DELETE'])]
    public function show(int $id, Request $request): Response
    {
        $product = $this->productRepository->find($id);
        if (null === $product) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        if ('PUT' === $request->getMethod()) {
            return $this->manageProductRequest($product, $request);
        }

        if ('DELETE' === $request->getMethod()) {
            $this->entityManager->remove($product);
            $this->entityManager->flush();

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        }

        return $this->JsonProductResponse($product);
    }

    private function manageProductRequest(Product $product, Request $request): Response
    {
        $form = $this->createForm(ProductFormType::class, $product);

        // Decode de JSON input
        $data = json_decode($request->getContent(), true);

        // Post the data to the form
        $form->submit($data);

        if ($form->isValid()) {
            $product = $form->getData();
            $this->entityManager->persist($product);
            $this->entityManager->flush();

            return $this->JsonProductResponse($product);
        }

        return $this->JsonFormResponse($form, Response::HTTP_BAD_REQUEST);
    }
}
