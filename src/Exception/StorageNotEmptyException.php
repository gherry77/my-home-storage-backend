<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;
use Symfony\Component\HttpFoundation\Response;

final class StorageNotEmptyException extends Exception
{
    const MESSAGE = 'Storage not empty';

    /** @var int */
    protected $code = Response::HTTP_INTERNAL_SERVER_ERROR;

    /** @var string */
    protected $message = self::MESSAGE;
}
