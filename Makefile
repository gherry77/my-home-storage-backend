SHELL := /bin/bash

tests: export APP_ENV=test
tests:
	symfony console doctrine:database:drop --force || true
	symfony console doctrine:database:create
	symfony console doctrine:migrations:migrate -n
	# symfony console doctrine:fixtures:load -n
	symfony php bin/phpunit $@
.PHONY: tests

clear-db-test: export APP_ENV=test
clear-db-test:
	symfony console doctrine:database:drop --force || true
	symfony console doctrine:database:create
	symfony console doctrine:migrations:migrate -n
	# symfony console doctrine:fixtures:load -n
.PHONY: clear-db-test

first-start-up:
	php bin/console doctrine:database:create
	php bin/console doctrine:migrations:migrate -n
	php bin/console doctrine:fixtures:load -n
.PHONY: first-start-up