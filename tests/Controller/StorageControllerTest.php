<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\Storage;
use App\Exception\StorageNotEmptyException;
use App\Tests\BaseApiTestCase;
use DateInterval;
use Symfony\Component\HttpFoundation\Response;

class StorageControllerTest extends BaseApiTestCase
{
    public function test_get_empty_storage_list(): void
    {
        $response = static::createClient()->request('GET', '/storages');

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([]);
    }

    public function test_get_storage_list(): void
    {
        $s1 = $this->buildStorage();

        $this->em->persist($s1);
        $this->em->flush();

        $response = static::createClient()->request('GET', '/storages', ['headers' => ['accept' => 'application/json']]);

        $this->assertResponseIsSuccessful();
        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);
        $this->assertEquals(1, \count($jsonResponseDecoded));
        $this->assertEquals($s1->getName(), $jsonResponseDecoded[0]['name']);
    }

    public function test_get_storage_item(): void
    {
        $s1 = $this->buildStorage();

        $this->em->persist($s1);
        $this->em->flush();

        $response = static::createClient()->request('GET', '/storages/'.$s1->getId());

        $this->assertResponseIsSuccessful();
    }

    public function test_get_storage_item_not_found(): void
    {
        $response = static::createClient()->request('GET', '/storages/'.random_int(1, 10));

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function test_post_storage_item(): void
    {
        $s1 = $this->buildStorage();

        $jsonPost = json_encode([
            'name' => $s1->getName(),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/storages',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($s1->getName(), $jsonResponseDecoded['name']);
    }

    public function test_post_storage_item_with_wrong_name(): void
    {
        $jsonPost = json_encode([
            'name' => null,
        ]);

        $response = static::createClient()->request(
            'POST',
            '/storages',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $jsonResponse = $response->getContent(false);
        $this->assertJsonContains(['title' => 'Validation Failed']);
        $this->assertStringContainsString('"name":{"errors":[{"message":"This value should not be blank."', $jsonResponse);
    }

    public function test_post_storage_item_already_exists(): void
    {
        $s1 = $this->buildStorage();
        $this->em->persist($s1);
        $this->em->flush();

        $jsonPost = json_encode([
            'name' => $s1->getName(),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/storages',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $jsonResponse = $response->getContent(false);
        $this->assertJsonContains(['title' => 'Validation Failed']);
        $this->assertStringContainsString('"name":{"errors":[{"message":"This value is already used."', $jsonResponse);
    }

    public function test_update_storage_item(): void
    {
        $s1 = $this->buildStorage();

        $this->em->persist($s1);
        $this->em->flush();

        $s1->setName($nameUpdated = 'prova');

        $jsonPut = json_encode([
            'name' => $s1->getName(),
        ]);

        $response = static::createClient()->request(
            'PUT',
            '/storages/'.$s1->getId(),
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPut,
            ]
        );

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($nameUpdated, $jsonResponseDecoded['name']);
    }

    public function test_delete_storage_item(): void
    {
        $s1 = $this->buildStorage();

        $this->em->persist($s1);
        $this->em->flush();

        $response = static::createClient()->request(
            'DELETE',
            '/storages/'.$s1->getId(),
            [
                'headers' => ['accept' => 'application/json'],
            ]
        );

        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        $storage = $this->em->getRepository(Storage::class)->find($s1->getId());
        $this->assertNull($storage);
    }

    public function test_delete_storage_item_not_found(): void
    {
        $response = static::createClient()->request(
            'DELETE',
            '/storages/'.random_int(1, 100),
            [
                'headers' => ['accept' => 'application/json'],
            ]
        );

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function test_delete_storage_item_with_products(): void
    {
        $s1 = $this->buildStorage();

        $this->em->persist($s1);
        $this->em->flush();

        for ($i = 0; $i < 3; ++$i) {
            $p1 = $this->buildProductWithStorage($s1);
            $this->em->persist($p1);
            $this->em->flush();
        }

        $response = static::createClient()->request(
            'DELETE',
            '/storages/'.$s1->getId(),
            [
                'headers' => ['accept' => 'application/json'],
            ]
        );

        $jsonResponse = $response->getContent(false);

        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        $this->assertStringContainsString(StorageNotEmptyException::MESSAGE, $jsonResponse);

        $storage = $this->em->getRepository(Storage::class)->find($s1->getId());
        $this->assertEquals($s1->getId(), $storage->getId());
    }

    public function test_get_storage_statistics(): void
    {
        $s1 = $this->buildStorage();
        $this->em->persist($s1);
        $this->em->flush();

        $expiredDate = (new \DateTime())->sub(new DateInterval('P1D'));
        $expiringSoonDate = (new \DateTime())->add(new DateInterval('P2D'));
        $notExpiredDate = (new \DateTime())->add(new DateInterval('P30D'));

        $p1 = $this->buildProductWithStorage($s1);
        $p1->setExpiryDate($expiredDate);
        $this->em->persist($p1);

        $p2 = $this->buildProductWithStorage($s1);
        $p2->setExpiryDate($expiredDate);
        $this->em->persist($p2);

        $p3 = $this->buildProductWithStorage($s1);
        $p3->setExpiryDate($expiringSoonDate);
        $this->em->persist($p3);

        $p4 = $this->buildProductWithStorage($s1);
        $p4->setExpiryDate($notExpiredDate);
        $this->em->persist($p4);

        $this->em->flush();

        $response = static::createClient()->request('GET', '/storages/'.$s1->getId().'/statistics');

        $this->assertResponseIsSuccessful();

        $expected = '{"expired":2,"expiring_soon":1,"not_expired":1,"total":4}';
        $this->assertEquals($expected, $response->getContent());
    }

    public function test_get_storage_statistics_item_not_found(): void
    {
        $response = static::createClient()->request('GET', '/storages/'.random_int(1, 10).'/statistics');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function test_get_storage_products_item_not_found(): void
    {
        $response = static::createClient()->request('GET', '/storages/'.random_int(1, 10).'/products');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function test_get_storage_products(): void
    {
        $s1 = $this->buildStorage();
        $this->em->persist($s1);
        $this->em->flush();

        $expiredDate = (new \DateTime())->sub(new DateInterval('P1D'));
        $expiringSoonDate = (new \DateTime())->add(new DateInterval('P2D'));
        $notExpiredDate = (new \DateTime())->add(new DateInterval('P30D'));

        $p1 = $this->buildProductWithStorage($s1);
        $p1->setExpiryDate($expiredDate);
        $this->em->persist($p1);

        $p2 = $this->buildProductWithStorage($s1);
        $p2->setExpiryDate($expiredDate);
        $this->em->persist($p2);

        $p3 = $this->buildProductWithStorage($s1);
        $p3->setExpiryDate($expiringSoonDate);
        $this->em->persist($p3);

        $p4 = $this->buildProductWithStorage($s1);
        $p4->setExpiryDate($notExpiredDate);
        $this->em->persist($p4);

        $productWithoutStorage = $this->buildProduct();
        $this->em->persist($productWithoutStorage);

        $this->em->flush();

        $response = static::createClient()->request('GET', '/storages/'.$s1->getId().'/products');

        $this->assertResponseIsSuccessful();

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(4, \count($jsonResponseDecoded));
    }

    public function test_get_storage_products_filtered_by_description(): void
    {
        $expected = 0;

        $s1 = $this->buildStorage();
        $this->em->persist($s1);
        $this->em->flush();

        $s2 = $this->buildStorage();
        $this->em->persist($s2);
        $this->em->flush();

        for ($i = 1; $i <= 3; ++$i) {
            $p1 = $this->buildProductWithStorage($s1);
            if ($i % 2) {
                $p1->setDescription('AAAaaaAAA'.$i);
                ++$expected;
            }
            $this->em->persist($p1);
            $this->em->flush();
        }

        for ($i = 1; $i <= 2; ++$i) {
            $p1 = $this->buildProductWithStorage($s2);
            $p1->setDescription('BBBaaaBBB'.$i);
            $this->em->persist($p1);
            $this->em->flush();
        }

        $response = static::createClient()->request(
            'GET',
            '/storages/'.$s1->getId().'/products?description=aaa'
        );

        $this->assertResponseIsSuccessful();

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($expected, \count($jsonResponseDecoded));
    }
}
