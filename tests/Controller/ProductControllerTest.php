<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\Product;
use App\Tests\BaseApiTestCase;
use DateInterval;
use DateTime;
use Symfony\Component\HttpFoundation\Response;

class ProductControllerTest extends BaseApiTestCase
{
    public function test_get_empty_product_list(): void
    {
        $response = static::createClient()->request('GET', '/products');

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([]);
    }

    public function test_get_product_list(): void
    {
        $p1 = $this->buildProduct();

        $this->em->persist($p1);
        $this->em->flush();

        $response = static::createClient()->request('GET', '/products', ['headers' => ['accept' => 'application/json']]);

        $this->assertResponseIsSuccessful();
        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);
        $this->assertEquals(1, \count($jsonResponseDecoded));
        $this->assertEquals($p1->getDescription(), $jsonResponseDecoded[0]['description']);
    }

    public function test_get_product_list_filtered_by_storage(): void
    {
        $s1 = $this->buildStorage();
        $this->em->persist($s1);
        $this->em->flush();

        $s2 = $this->buildStorage();
        $this->em->persist($s2);
        $this->em->flush();

        for ($i = 1; $i <= 3; ++$i) {
            $p1 = $this->buildProductWithStorage($s1);
            $this->em->persist($p1);
            $this->em->flush();
        }

        for ($i = 1; $i <= 2; ++$i) {
            $p1 = $this->buildProductWithStorage($s2);
            $this->em->persist($p1);
            $this->em->flush();
        }

        $response = static::createClient()->request(
            'GET',
            '/products?storage='.$s1->getId(),
            ['headers' => ['accept' => 'application/json']]
        );

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(3, \count($jsonResponseDecoded));
    }

    public function test_get_product_list_filtered_by_decription_and_storage(): void
    {
        $expected = 0;

        $s1 = $this->buildStorage();
        $this->em->persist($s1);
        $this->em->flush();

        $s2 = $this->buildStorage();
        $this->em->persist($s2);
        $this->em->flush();

        for ($i = 1; $i <= 3; ++$i) {
            $p1 = $this->buildProductWithStorage($s1);
            if ($i % 2) {
                $p1->setDescription('AAAaaaAAA'.$i);
                ++$expected;
            }
            $this->em->persist($p1);
            $this->em->flush();
        }

        for ($i = 1; $i <= 2; ++$i) {
            $p1 = $this->buildProductWithStorage($s2);
            $p1->setDescription('BBBaaaBBB'.$i);
            $this->em->persist($p1);
            $this->em->flush();
        }

        $response = static::createClient()->request(
            'GET',
            '/products?description=aaa&storage='.$s1->getId(),
            ['headers' => ['accept' => 'application/json']]
        );

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertNotEquals(3, \count($jsonResponseDecoded));
        $this->assertEquals($expected, \count($jsonResponseDecoded));
    }

    public function test_get_product_item(): void
    {
        $p1 = $this->buildProduct();

        $this->em->persist($p1);
        $this->em->flush();

        $response = static::createClient()->request('GET', '/products/'.$p1->getId());

        $this->assertResponseIsSuccessful();
    }

    public function test_get_product_item_with_expiry_date(): void
    {
        $p1 = $this->buildProduct();
        $p1->setExpiryDate((new \DateTime())->setTime(0, 0, 0));

        $this->em->persist($p1);
        $this->em->flush();

        $response = static::createClient()->request('GET', '/products/'.$p1->getId());

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($p1->getDescription(), $jsonResponseDecoded['description']);
        $this->assertEquals($p1->getExpiryDate()->format('Y-m-d'), $jsonResponseDecoded['expiry_date']);
    }

    public function test_get_product_item_not_found(): void
    {
        $response = static::createClient()->request('GET', '/products/'.random_int(1, 10));

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function test_post_product_item(): void
    {
        $p1 = $this->buildProduct();

        $jsonPost = json_encode([
            'description' => $p1->getDescription(),
            'expiry_date' => $p1->getExpiryDate(),
            'quantity' => $p1->getQuantity(),
            'storage' => $p1->getStorage(),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/products',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($p1->getDescription(), $jsonResponseDecoded['description']);
    }

    public function test_post_product_item_with_storage(): void
    {
        $s1 = $this->buildStorage();
        $this->em->persist($s1);
        $this->em->flush();

        $p1 = $this->buildProductWithStorage($s1);

        $jsonPost = json_encode([
            'description' => $p1->getDescription(),
            'expiry_date' => $p1->getExpiryDate(),
            'quantity' => $p1->getQuantity(),
            'storage' => $p1->getStorage()->getId(),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/products',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($p1->getDescription(), $jsonResponseDecoded['description']);
        $this->assertEquals($p1->getStorage()->getId(), $jsonResponseDecoded['storage']['id']);
    }

    public function test_post_product_item_with_expiry_date(): void
    {
        //$expiryDate = (new \DateTime())->format(DateTime::ATOM);
        //$expiryDate = "2021-10-09T00:00:00+0000";
        //$expiryDate = "2021-10-09";
        $expiryDate = (new \DateTime())->format('Y-m-d');

        $jsonPost = json_encode([
            'description' => $description = uniqid(),
            'expiry_date' => $expiryDate,
            'quantity' => '1',
            'storage' => null,
        ]);

        $response = static::createClient()->request(
            'POST',
            '/products',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $jsonResponse = $response->getContent(false);
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($description, $jsonResponseDecoded['description']);
    }

    public function test_post_product_item_with_wrong_description(): void
    {
        $p1 = $this->buildProduct();

        $jsonPost = json_encode([
            'description' => null,
            'expiry_date' => $p1->getExpiryDate(),
            'quantity' => $p1->getQuantity(),
            'storage' => $p1->getStorage(),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/products',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $jsonResponse = $response->getContent(false);
        $this->assertJsonContains(['title' => 'Validation Failed']);
        $this->assertStringContainsString('"description":{"errors":[{"message":"This value should not be blank."', $jsonResponse);
    }

    public function test_post_product_item_with_wrong_expiry_date(): void
    {
        $p1 = $this->buildProduct();

        $jsonPost = json_encode([
            'description' => $p1->getDescription(),
            'expiry_date' => uniqid(),
            'quantity' => $p1->getQuantity(),
            'storage' => $p1->getStorage(),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/products',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $jsonResponse = $response->getContent(false);
        $this->assertJsonContains(['title' => 'Validation Failed']);
        $this->assertStringContainsString('"expiry_date":{"errors":[{"message":"This value is not valid."', $jsonResponse);
    }

    public function test_post_product_item_with_wrong_quantity(): void
    {
        $p1 = $this->buildProduct();

        $jsonPost = json_encode([
            'description' => $p1->getDescription(),
            'expiry_date' => $p1->getExpiryDate(),
            'quantity' => -1,
            'storage' => $p1->getStorage(),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/products',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $jsonResponse = $response->getContent(false);
        $this->assertJsonContains(['title' => 'Validation Failed']);
        $this->assertStringContainsString('"quantity":{"errors":[{"message":"This value should be either positive or zero."', $jsonResponse);
    }

    public function test_post_product_item_with_wrong_storage(): void
    {
        $p1 = $this->buildProduct();

        $jsonPost = json_encode([
            'description' => $p1->getDescription(),
            'expiry_date' => $p1->getExpiryDate(),
            'quantity' => $p1->getQuantity(),
            'storage' => uniqid(),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/products',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $jsonResponse = $response->getContent(false);
        $this->assertJsonContains(['title' => 'Validation Failed']);
        $this->assertStringContainsString('"storage":{"errors":[{"message":"This value is not valid."', $jsonResponse);
    }

    public function test_post_product_item_with_inexistent_storage(): void
    {
        $p1 = $this->buildProduct();

        $jsonPost = json_encode([
            'description' => $p1->getDescription(),
            'expiry_date' => $p1->getExpiryDate(),
            'quantity' => $p1->getQuantity(),
            'storage' => random_int(1, 10),
        ]);

        $response = static::createClient()->request(
            'POST',
            '/products',
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPost,
            ]
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $jsonResponse = $response->getContent(false);
        $this->assertJsonContains(['title' => 'Validation Failed']);
        $this->assertStringContainsString('"storage":{"errors":[{"message":"This value is not valid."', $jsonResponse);
    }

    public function test_update_product_item(): void
    {
        $p1 = $this->buildProduct();

        $this->em->persist($p1);
        $this->em->flush();

        $p1->setDescription($descriptionUpdated = 'prova');

        $jsonPut = json_encode([
            'description' => $p1->getDescription(),
            'expiry_date' => $p1->getExpiryDate(),
            'quantity' => $p1->getQuantity(),
            'storage' => null,
        ]);

        $response = static::createClient()->request(
            'PUT',
            '/products/'.$p1->getId(),
            [
                'headers' => ['accept' => 'application/json'],
                'body' => $jsonPut,
            ]
        );

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($descriptionUpdated, $jsonResponseDecoded['description']);
    }

    public function test_delete_product_item(): void
    {
        $p1 = $this->buildProduct();

        $this->em->persist($p1);
        $this->em->flush();

        $response = static::createClient()->request(
            'DELETE',
            '/products/'.$p1->getId(),
            [
                'headers' => ['accept' => 'application/json'],
            ]
        );

        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        $product = $this->em->getRepository(Product::class)->find($p1->getId());
        $this->assertNull($product);
    }

    public function test_delete_product_item_not_found(): void
    {
        $response = static::createClient()->request(
            'DELETE',
            '/products/'.random_int(1, 100),
            [
                'headers' => ['accept' => 'application/json'],
            ]
        );

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function test_get_product_ordered_by_expiring_days(): void
    {
        $products = [];

        $expiredDate = (new \DateTime())->sub(new DateInterval('P1D'));
        $expiringSoonDate = (new \DateTime())->add(new DateInterval('P2D'));
        $notExpiredDate = (new \DateTime())->add(new DateInterval('P30D'));

        $products[] = ($this->buildProduct())->setDescription('senza scadenza');
        $products[] = ($this->buildProduct())->setExpiryDate($notExpiredDate)->setDescription('non scaduto');
        $products[] = ($this->buildProduct())->setExpiryDate($expiringSoonDate)->setDescription('in scadenza');
        $products[] = ($this->buildProduct())->setExpiryDate($expiredDate)->setDescription('bbb');
        $products[] = ($this->buildProduct())->setExpiryDate($expiredDate)->setDescription('aaa');

        foreach ($products as $product) {
            $this->em->persist($product);
        }
        $this->em->flush();

        $response = static::createClient()->request('GET', '/products');

        $this->assertResponseIsSuccessful();

        $jsonResponse = $response->getContent();
        $jsonResponseDecoded = json_decode($jsonResponse, true);

        // var_dump($jsonResponse);
        //var_dump($jsonResponseDecoded);

        $this->assertEquals(\count($products), \count($jsonResponseDecoded));
        $this->assertArrayHasKey('expiring_days', $jsonResponseDecoded[0]);

        $this->assertEquals(
            $products[0]->getExpiringDays(),
            $jsonResponseDecoded[\count($products) - 1]['expiring_days']
        );

        $this->assertEquals(
            'aaa',
            $jsonResponseDecoded[0]['description']
        );
        $this->assertEquals(
            'bbb',
            $jsonResponseDecoded[1]['description']
        );
    }
}
