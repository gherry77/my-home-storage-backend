<?php
declare(strict_types=1);

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Product;
use App\Entity\Storage;
use Doctrine\ORM\EntityManagerInterface;

class BaseApiTestCase extends ApiTestCase
{
    protected EntityManagerInterface $em;

    protected function setUp(): void
    {
        parent::setUp();

        $this->em = static::getContainer()->get(EntityManagerInterface::class);
        $this->givenProductsNoData();
        $this->givenStoragesNoData();
    }

    protected function givenStoragesNoData(): void
    {
        $this->em->createQuery('delete from App\Entity\Storage')->execute();
    }

    protected function givenProductsNoData(): void
    {
        $this->em->createQuery('delete from App\Entity\Product')->execute();
    }

    protected function buildProduct(): Product
    {
        $p1 = new Product();
        $p1->setDescription(uniqid());
        $p1->setQuantity('1.00');

        return $p1;
    }

    protected function buildProductWithStorage(Storage $s1): Product
    {
        $p1 = $this->buildProduct();
        $p1->setStorage($s1);

        return $p1;
    }

    protected function buildStorage(): Storage
    {
        $s1 = new Storage();
        $s1->setName(uniqid());

        return $s1;
    }
}
