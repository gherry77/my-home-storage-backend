<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Product;
use DateInterval;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /**
     * @dataProvider dataProviderExpiringDays
     *
     * @param ?int               $expected
     * @param ?DateTimeInterface $expiryDate
     */
    public function test_product_expiring_days(?int $expected, ?DateTimeInterface $expiryDate): void
    {
        $p1 = new Product();

        $p1->setExpiryDate($expiryDate);

        $this->assertEquals($expected, $p1->getExpiringDays());
    }

    /**
     * @return array<array>
     */
    public function dataProviderExpiringDays(): array
    {
        return [
            [null, null],
            [0, new \DateTime()],
            [1, (new \DateTime())->add(new DateInterval('P1DT1M'))],
            [-1, (new \DateTime())->sub(new DateInterval('P1DT1M'))],
        ];
    }
}
