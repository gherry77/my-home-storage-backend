<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Product;
use App\Service\StorageStatistics;
use DateInterval;
use PHPUnit\Framework\TestCase;

class StorageStatisticsTest extends TestCase
{
    public function test_empty_statistics(): void
    {
        $expected = '{"expired":0,"expiring_soon":0,"not_expired":0,"total":0}';

        $storageStatistics = new StorageStatistics();
        $statistics = $storageStatistics->getStatistics();
        $this->assertEquals($expected, $statistics);
    }

    public function test_statistics(): void
    {
        $expected = '{"expired":2,"expiring_soon":1,"not_expired":1,"total":4}';

        $products = [];

        $expiredDate = (new \DateTime())->sub(new DateInterval('P1D'));
        $expiringSoonDate = (new \DateTime())->add(new DateInterval('P2D'));
        $notExpiredDate = (new \DateTime())->add(new DateInterval('P30D'));

        $p1 = new Product();
        $p1->setExpiryDate($expiredDate);
        $products[] = $p1;

        $p2 = new Product();
        $p2->setExpiryDate($expiredDate);
        $products[] = $p2;

        $p3 = new Product();
        $p3->setExpiryDate($expiringSoonDate);
        $products[] = $p3;

        $p4 = new Product();
        $p4->setExpiryDate($notExpiredDate);
        $products[] = $p4;

        $storageStatistics = new StorageStatistics($products);
        $statistics = $storageStatistics->getStatistics();
        $this->assertEquals($expected, $statistics);
    }
}
