<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Product;
use App\Service\ProductReport;
use DateInterval;
use PHPUnit\Framework\TestCase;

class ProductReportTest extends TestCase
{
    public function test_empty_report(): void
    {
        $expected = [];

        $productReport = new ProductReport();
        $report = $productReport->getReport();
        $this->assertEquals($expected, $report);
    }

    public function test_report(): void
    {
        $products = [];

        $expiredDate = (new \DateTime())->sub(new DateInterval('P1D'));
        $expiringSoonDate = (new \DateTime())->add(new DateInterval('P2D'));
        $notExpiredDate = (new \DateTime())->add(new DateInterval('P30D'));

        $products[] = (new Product())->setDescription('senza scadenza');
        $products[] = (new Product())->setExpiryDate($notExpiredDate)->setDescription('non scaduto');
        $products[] = (new Product())->setExpiryDate($expiringSoonDate)->setDescription('in scadenza');
        $products[] = (new Product())->setExpiryDate($expiredDate)->setDescription('bbb');
        $products[] = (new Product())->setExpiryDate($expiredDate)->setDescription('aaa');

        $productReport = new ProductReport($products);
        $report = $productReport->getReport();
        $numProducts = \count($products);
        $this->assertEquals($numProducts, \count($report));

        //var_dump($report);
        $this->assertEquals(
            $products[0]->getExpiryDate(),
            $report[$numProducts - 1]->getExpiryDate()
        );
        $this->assertEquals(
            'aaa',
            $report[0]->getDescription()
        );
        $this->assertEquals(
            'bbb',
            $report[1]->getDescription()
        );
    }
}
