<?php
declare(strict_types=1);

namespace App\Tests\Form;

use App\Entity\Product;
use App\Form\ProductFormType;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\FormFactory;

class ProductFormTypeTest extends KernelTestCase
{
    public function test_something(): void
    {
        $expiryDate = (new \DateTime())->format('Y-m-d');

        $formData = [
            'description' => 'aaa',
            'quantity' => null,
            //'expiry_date' => 1,
            //'expiry_date' => (new \DateTime())->format(\DateTime::ATOM),
            'expiry_date' => $expiryDate,
        ];

        $factory = $this->getFormFactory();
        $product = new Product();
        $form = $factory->create(ProductFormType::class, $product, []);
        $form->submit($formData);

        //var_dump((string) $form->getErrors(true, false));

        $this->assertTrue($form->isValid());
    }

    protected function getFormFactory(): FormFactory
    {
        return static::getContainer()->get('form.factory');
    }
}
