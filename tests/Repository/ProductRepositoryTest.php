<?php
declare(strict_types=1);

namespace App\Tests\Repository;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Tests\BaseApiTestCase;

class ProductRepositoryTest extends BaseApiTestCase
{
    public function test_product_repository_filter_by_description(): void
    {
        /** @var ProductRepository */
        $productRepository = $this->em->getRepository(Product::class);

        //### Products ###
        $p1 = $this->buildProduct();
        $p1->setDescription('AAAaaaAAA');

        $this->em->persist($p1);
        $this->em->flush();

        $p2 = $this->buildProduct();
        $p2->setDescription('bbb');

        $this->em->persist($p2);
        $this->em->flush();

        $description = 'aaa';

        $products = $productRepository->findByDescriptionAndStorage($description);

        $this->assertEquals(1, \count($products));
    }

    public function test_product_repository_filter_by_description_and_storage(): void
    {
        /** @var ProductRepository */
        $productRepository = $this->em->getRepository(Product::class);

        //### Storage ###
        $s1 = $this->buildStorage();
        $this->em->persist($s1);
        $this->em->flush();

        //### Products ###
        $p1 = $this->buildProductWithStorage($s1);
        $p1->setDescription('AAAaaaAAA');

        $this->em->persist($p1);
        $this->em->flush();

        $p2 = $this->buildProduct();
        $p2->setDescription('BBBaaaBBB');

        $this->em->persist($p2);
        $this->em->flush();

        $description = 'aaa';

        $products = $productRepository->findByDescriptionAndStorage($description, $s1);

        $this->assertEquals(1, \count($products));
    }
}
