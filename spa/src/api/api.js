function fetchCollection(path) {
    //return fetch(ENV_API_ENDPOINT + path).then(resp => resp.json()).then(json => json['hydra:member']);
    return fetch(ENV_API_ENDPOINT + path).then(resp => resp.json()).then(json => json);
}

export function findStorages() {
    return fetchCollection('storages');
}

export function findStorageStatistics(storage) {
    return fetchCollection('storages/'+storage.id+'/statistics');
}

export function findStorageProducts(storage) {
    return fetchCollection('products?storage='+storage.id);
    //return fetchCollection('storages/'+storage.id+'/report');
}

export function findProducts() {
    return fetchCollection('products');
}
