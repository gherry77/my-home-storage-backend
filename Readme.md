# My Home Storage Backend

This project is an example to store food

## Requirements

- Docker
- PHP 8.0
- Symfony CLI

## Install

### 1. clone the project and enter into the project folder

```
$ git clone https://gitlab.com/gherry77/my-home-storage-backend.git
$ cd my-home-storage-backend
```

### 2. run docker-compose
Run docker compose to create PostGres database container
```
$ docker-compose up -d
```

### 3. Install dependecies with composer
```
$ symfony composer install
```

### 4. Copy `.env.example` to `.env.local`
```
$ cp .env.example .env.local
```

### 5. Create Tables
```
$ symfony console doctrine:migrations:migrate -n
```

### 6. Load Fixtures
Run this command only locally to load fixtures.  
Be careful the database will be purged, so your previuos data will be lost  
```
symfony console doctrine:fixtures:load
```

### 7. Start server local
Start server in background
```
$ symfony server:start -d
```

### 8. Open server local with browser
```
$ symfony open:local
```

### 9. You can open API
- Product List  
[GET] http://localhost:8000/products  

- Storage List  
[GET] http://localhost:8000/storages  
  
## Api Platform
Otherwise you can open this link to see api with api platform

http://localhost:8000/api  


## Test
These are the instructions to run test locally

### 1. Copy `.env.example` to `.env.local.test`
Copy file to set right DATABASE_URL for testing locally  
Note: Symfony uses `_test` suffix for db name  
```
$ cp .env.example .env.test.local
```

### 2. run test script
```
$ ./runtests
```

# Frontend

## Requisites

- Yarn

## Install
In folder spa there is frontend project. This is an example of SPA written in preact.  
From the project dir

### 1. Install dependecies
```
cd spa
yarn install
```

### 2. Build  
Build passing env var API_ENPOINT
```
API_ENDPOINT=`symfony var:export SYMFONY_PROJECT_DEFAULT_ROUTE_URL --dir=..` yarn encore dev
```

### 3. Run locally
```
symfony server:start -d --passthru=index.html
```

### 4. Open server locally
```
symfony open:local
```

![frontend screenshot](frontend.png "Frontend")